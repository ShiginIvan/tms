from django.db import models


class TestCase(models.Model):
    test_name = models.CharField(max_length=200, verbose_name='Название теста')
    test_dir = models.CharField(max_length=200, verbose_name='Описание теста')
    test_rest = models.CharField(max_length=20, verbose_name='Рест', null=True)
    test_sql = models.CharField(max_length=20, verbose_name='SQL', null=True)
    test_expect = models.CharField(max_length=20, verbose_name='Ожидаемый результат')
    test_result = models.CharField(max_length=20, verbose_name='Фактический')
    test_note = models.CharField(max_length=20, verbose_name='Примечание')
    data = models.DateTimeField(auto_now=True)
    jira = models.CharField(max_length=30, verbose_name='ССЫЛКА JIRA', null=True)

    def __str__(self):
        return self.test_name

    class Meta:
        verbose_name = 'Тест-кейс'
        verbose_name_plural = 'Тест-кейсы'
