from django import forms


class OrderForm(forms.Form):
    name = forms.CharField(max_length=10)
    discr = forms.CharField(max_length=10)
    res = forms.CharField(max_length=10)