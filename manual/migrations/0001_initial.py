# Generated by Django 4.1.3 on 2022-11-25 19:41

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TestCase',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_name', models.CharField(max_length=200, verbose_name='Название теста')),
                ('test_dir', models.CharField(max_length=200, verbose_name='Описание теста')),
                ('test_result', models.CharField(max_length=20, verbose_name='Результат')),
            ],
            options={
                'verbose_name': 'Тест-кейс',
                'verbose_name_plural': 'Тест-кейсы',
            },
        ),
    ]
