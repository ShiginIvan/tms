# Generated by Django 4.1.3 on 2022-11-26 01:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manual', '0008_testcase_jira'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testcase',
            name='jira',
            field=models.CharField(max_length=30, verbose_name='ССЫЛКА JIRA'),
        ),
    ]
