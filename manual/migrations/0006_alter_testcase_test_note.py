# Generated by Django 4.1.3 on 2022-11-26 00:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('manual', '0005_testcase_test_note'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testcase',
            name='test_note',
            field=models.CharField(max_length=20, verbose_name='Примечание'),
        ),
    ]
