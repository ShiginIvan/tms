from django.http import HttpResponseRedirect
from django.shortcuts import render

from manual.forms import OrderForm
from manual.models import TestCase


# Create your views here.
def first_page(request):
    return render(request, 'index.html')


def send(request):
    slider_list = TestCase.objects.all()
    return render(request, 'results.html', {'slider_list': slider_list})


def complete(request):
    return render(request, 'complete.html')


def create(request):
    if request.method == 'POST':
        name = request.POST['name']
        discr = request.POST['dir']
        res = request.POST['res']
        element = TestCase(test_name=name, test_dir=discr, test_result=res)
        element.save()
        return HttpResponseRedirect('/complete/')
    else:
        return render(request, 'create.html')


def create_micro(request):
    if request.method == 'POST':
        name = request.POST['name']
        discr = request.POST['dir']
        rest = request.POST['rest']
        sql = request.POST['sql']
        expect = request.POST['expect']
        res = request.POST['res']
        note = request.POST['note']
        jira = request.POST['jira']
        element = TestCase(test_name=name, test_dir=discr, test_result=res, test_rest=rest, test_sql=sql,
                           test_expect=expect, test_note=note, jira=jira)
        element.save()
        return HttpResponseRedirect('/complete/')
    else:
        return render(request, 'micro.html')
